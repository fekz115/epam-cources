var thisUser = 0;

var teachers;
var schedule;
var subjects;
var permissions;
var teachersRang;
var subjectsType;

var strings;

function Table(objectsArray, headers, getTableLine, ReplaceFieldsForEditFunc, ReplaceFieldsFunc, ObjectForRequest, requests, canCreate){

    this.objectArray = objectsArray;
    this.getTableLine = getTableLine;
    this.headers = headers;
    this.requests = requests;
    this.getReplaceFieldsForEditFunc = ReplaceFieldsForEditFunc;
    this.getReplaceFieldsFunc = ReplaceFieldsFunc;
    this.getObjectForRequest = ObjectForRequest;

    this.getTr = function () {
        var ans = createTableHeader(headers);
        ans.appendChild(document.createElement('td'));
        return ans;
    };

    this.getTableLines = function () {
        objectsArray = check(objectsArray, requests.readRequest());
        var ans = [];
        objectsArray.forEach(function (item) {
            ans.push(getTableLine(item, this));
        });
        if(canCreate == true){
            ans.push(createEntryButton(this));
        }
        return ans;
    };
}

function Request(createRequest, readRequest, updateRequest, deleteRequest, fieldsToClear){
    this.readRequest = readRequest;
    this.createRequest = createRequest;
    this.updateRequest = updateRequest;
    this.deleteRequest = deleteRequest;
    this.fieldsToClear = fieldsToClear;
}

function drawTable(table) {
    var ans = document.createElement('table');
    ans.appendChild(table.getTr());
    table.getTableLines().forEach(function (item) {
        ans.appendChild(item);
    });
    var forReplace = document.getElementById('forReplace');
    ans.id = 'forReplace';
    ans.classList.add("contentTable");
    forReplace.parentNode.replaceChild(ans, forReplace);
}

function createTableHeader(titles) {
    var tr = document.createElement('tr');
    titles.forEach(function (item) {
        var th = document.createElement('th');
        th.appendChild(document.createTextNode(item));
        tr.appendChild(th);
    });
    return tr;
}

function createTableLine(fields) {
    var tr = document.createElement('tr');
    fields.forEach(function (item) {
        var td = document.createElement('td');
        td.appendChild(document.createTextNode(item));
        tr.appendChild(td);
    });
    return tr;
}


function drawTeachersRangTable() {
    var table = new Table(teachersRang, [strings.column_name_teacher_rang],
        function (teacherRang) {
            var tr = createTableLine([teacherRang.rang]);
            tr.id = teacherRang.id;
            var edit = createEditButton(teacherRang.id, table);
            var deleteButton = createDeleteButton(teacherRang.id, table.requests.deleteRequest(teacherRang.id),  table.requests.fieldsToClear);
            var td = document.createElement('td');
            td.appendChild(edit);
            td.appendChild(deleteButton);
            tr.appendChild(td);
            return tr;
        }, function (oldTableLine) {
            var ans = document.createElement('td');
            var textField = document.createElement('input');
            textField.type = 'text';
            if(oldTableLine != null)textField.value =  oldTableLine.childNodes[0].textContent;
            ans.appendChild(textField);
            return ans;
        }, function (oldTableLine, newTableLine) {
            oldTableLine.childNodes[0].textContent = newTableLine.childNodes[0].childNodes[0].value;
        }, function (tableLine) {
            return {id:tableLine.id, rang:tableLine.childNodes[0].childNodes[0].value};
        }, new Request(
            function (teacherRang) {return 'createTeacherRang?rang=' + encodeURIComponent(teacherRang.rang)},
            function () {return 'getTeachersRang'},
            function (teacherRang) {return 'updateTeacherRang?id=' + encodeURIComponent(teacherRang.id) + '&rang=' + encodeURIComponent(teacherRang.rang);},
            function (id) {return 'deleteTeacherRang?id=' + id},
            [teachers, permissions]), true
    );
    drawTable(table);
}

function drawSubjectTypesTable() {
    var table = new Table(subjectsType, [strings.culumn_name_subject_type],
        function (subjectType) {
            var tr = createTableLine([subjectType.type]);
            tr.id = subjectType.id;
            var edit = createEditButton(subjectType.id, table);
            var deleteButton = createDeleteButton(subjectType.id, table.requests.deleteRequest(subjectType.id),  table.requests.fieldsToClear);
            var td = document.createElement('td');
            td.appendChild(edit);
            td.appendChild(deleteButton);
            tr.appendChild(td);
            return tr;
        }, function (oldTableLine) {
            var ans = document.createElement('td');
            var textField = document.createElement('input');
            textField.type = 'text';
            if(oldTableLine != null)textField.value =  oldTableLine.childNodes[0].textContent;
            ans.appendChild(textField);
            return ans;
        }, function (oldTableLine, newTableLine) {
            oldTableLine.childNodes[0].textContent = newTableLine.childNodes[0].childNodes[0].value;
        }, function (tableLine) {
            return {id:tableLine.id, type:tableLine.childNodes[0].childNodes[0].value};
        }, new Request(
            function (subjectType) {return 'createSubjectType?type=' + encodeURIComponent(subjectType.type)},
            function () {return 'getSubjectTypes'},
            function (subjectType) {return 'updateSubjectType?id=' + encodeURIComponent(subjectType.id) + '&type=' + encodeURIComponent(subjectType.type);},
            function (id) {return 'deleteSubjectType?id=' + id},
            [subjects, permissions]), true
    );
    drawTable(table);
}

function drawSubjectsTable() {
    var canCreate = false;
    if(thisUser == -2)canCreate = true;
    var table = new Table(subjects, [strings.column_name_subject, strings.culumn_name_subject_type, strings.column_name_teacher_name],
        function (subject) {
            var tr = createTableLine([subject.name, subject.type.type, subject.teacher.name]);
            tr.id = subject.id;
            tr.childNodes[1].id = subject.type.id;
            tr.childNodes[2].id = subject.teacher.id;
            var td = document.createElement('td');
            var edit = createEditButton(subject.id, table);
            td.appendChild(edit);
            if (thisUser == -2){
                var deleteButton = createDeleteButton(subject.id, table.requests.deleteRequest(subject.id), table.requests.fieldsToClear);
                td.appendChild(deleteButton);
            }
            tr.appendChild(td);
            return tr;
        }, function (oldTableLine) {
            subjectsType = check(subjectsType, 'getSubjectTypes');
            teachers = check(teachers, 'getTeachers');
            var ans = document.createElement('td');
            ans.colSpan = 3;
            var textField = document.createElement('input');
            textField.type = 'text';
            var select = document.createElement('select');
            var option;
            subjectsType.forEach(function (item) {
                option = document.createElement('option');
                option.value = item.id;
                option.textContent = item.type;
                select.appendChild(option);
            });
            if(oldTableLine != null) {
                option = document.createElement('option');
                option.value = oldTableLine.childNodes[1].id;
                option.textContent = oldTableLine.childNodes[1].textContent;
                select.appendChild(option);
            }
            option.selected = true;
            var selectTeacher = document.createElement('select');

            teachers.forEach(function (item) {
                option = document.createElement('option');
                option.value = item.id;
                option.textContent = item.name;
                selectTeacher.appendChild(option);
            });
            if(oldTableLine != null) {
                option = document.createElement('option');
                option.value = oldTableLine.childNodes[2].id;
                option.textContent = oldTableLine.childNodes[2].textContent;
                selectTeacher.appendChild(option);
            }
            option.selected = true;
            if(oldTableLine != null){
                textField.value =  oldTableLine.childNodes[0].textContent;
            }
            var v = document.createElement('table');
            var tr = document.createElement('tr');
            var td = document.createElement('td');
            td.appendChild(textField);
            tr.appendChild(td);
            td = document.createElement('td');
            td.appendChild(select);
            tr.appendChild(td);
            td = document.createElement('td');
            td.appendChild(selectTeacher);
            tr.appendChild(td);
            v.appendChild(tr);
            ans.appendChild(v);
            return ans;
        }, function (oldTableLine, newTableLine) {
            var subjectType = newTableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[1].childNodes[0];
            var selectedSubjectTypeIndex = subjectType.selectedIndex;
            var selectedSubjectType  = subjectType.options[selectedSubjectTypeIndex].textContent;
            var selectedSubjectTypeId = subjectType.value;
            var teacher = newTableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[2].childNodes[0];
            var selectedTeacherIndex = teacher.selectedIndex;
            var selectedTeacher  = teacher.options[selectedTeacherIndex].textContent;
            var selectedTeacherId = teacher.value;
            oldTableLine.childNodes[0].textContent = newTableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].value;
            oldTableLine.childNodes[1].textContent = selectedSubjectType;
            oldTableLine.childNodes[1].id = selectedSubjectTypeId;
            oldTableLine.childNodes[2].textContent = selectedTeacher;
            oldTableLine.childNodes[2].id = selectedTeacherId;
        }, function (tableLine) {
            var selectedSubjectTypeId = tableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[1].childNodes[0].value;
            var selectedTeacherId = tableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[2].childNodes[0].value;
            var ans = {id:tableLine.id, name:tableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].value, type:{id:selectedSubjectTypeId}, teacher:{id:selectedTeacherId}};
            return ans;
        }, new Request(
            function (subject) {return 'createSubject?name=' + encodeURIComponent(subject.name) + '&subject_type_id=' + encodeURIComponent(subject.type.id) + '&teacher_id=' + encodeURIComponent(subject.teacher.id);},
            function () {return 'getSubjects'},
            function (subject) {return 'updateSubject?id=' + encodeURIComponent(subject.id) + '&name=' + encodeURIComponent(subject.name) + '&subject_type_id=' + encodeURIComponent(subject.type.id) + '&teacher_id=' + encodeURIComponent(subject.teacher.id);},
            function (id) {return 'deleteSubject?id=' + id},
            [subjects, schedule]), canCreate
    );
    drawTable(table);
}


function drawTeachersTable() {
    var canCreate = false;
    if(thisUser == -2)canCreate = true;
    var table = new Table(teachers, [strings.column_name_teacher_name, strings.column_name_teacher_rang],
        function (teacher) {
            var tr = createTableLine([teacher.name, teacher.rang.rang]);
            tr.id = teacher.id;
            tr.childNodes[1].id = teacher.rang.id;
            var td = document.createElement('td');
            if(thisUser == -2) {
                var edit = createEditButton(teacher.id, table);
                var deleteButton = createDeleteButton(teacher.id, table.requests.deleteRequest(teacher.id), table.requests.fieldsToClear);
                td.appendChild(edit);
                td.appendChild(deleteButton);
            }
            tr.appendChild(td);
            return tr;
        }, function (oldTableLine) {
            teachersRang = check(teachersRang, 'getTeachersRang');
            var ans = document.createElement('td');
            ans.colSpan = 2;
            var textField = document.createElement('input');
            textField.type = 'text';
            var select = document.createElement('select');
            var option;
            teachersRang.forEach(function (item) {
                option = document.createElement('option');
                option.value = item.id;
                option.textContent = item.rang;
                select.appendChild(option);
            });
            if(oldTableLine != null) {
                option = document.createElement('option');
                option.value = oldTableLine.childNodes[1].id;
                option.textContent = oldTableLine.childNodes[1].textContent;
                select.appendChild(option);
            }
            option.selected = true;
            if(oldTableLine != null){
                textField.value =  oldTableLine.childNodes[0].textContent;
            }
            var v = document.createElement('table');
            var tr = document.createElement('tr');
            var td = document.createElement('td');
            td.appendChild(textField);
            tr.appendChild(td);
            td = document.createElement('td');
            td.appendChild(select);
            tr.appendChild(td);
            v.appendChild(tr);
            ans.appendChild(v);
            return ans;
        }, function (oldTableLine, newTableLine) {
            var teacher = newTableLine.childNodes[0].childNodes[0].childNodes[0];
            var selectedTeacherRangIndex = teacher.childNodes[1].childNodes[0].selectedIndex;
            var selectedTeacherRang = teacher.childNodes[1].childNodes[0][selectedTeacherRangIndex].textContent;
            oldTableLine.childNodes[0].textContent = teacher.childNodes[0].childNodes[0].value;
            oldTableLine.childNodes[1].textContent = selectedTeacherRang;
        }, function (tableLine) {
            var teacher = tableLine.childNodes[0].childNodes[0].childNodes[0];
            var ans = {id:tableLine.id, name:teacher.childNodes[0].childNodes[0].value, rang:{id:teacher.childNodes[1].childNodes[0].value}};
            return ans;
        }, new Request(
            function (teacher) {return 'createTeacher?name=' + encodeURIComponent(teacher.name) + '&teacher_rang_id=' + encodeURIComponent(teacher.rang.id);},
            function () {return 'getTeachers'},
            function (teacher) {return 'updateTeacher?id=' + encodeURIComponent(teacher.id) + '&name=' + encodeURIComponent(teacher.name) + '&teacher_rang_id=' + encodeURIComponent(teacher.rang.id);},
            function (id) {return 'deleteTeacher?id=' + id},
            [subjects, schedule]), canCreate
    );
    drawTable(table);
}


function drawSchedule() {
    var canCreate = false;
    if(thisUser == -2 || thisUser == -1)canCreate = true;
    var table = new Table(schedule, [strings.column_name_is_was, strings.column_name_date, strings.column_name_subject, strings.culumn_name_subject_type, strings.column_name_teacher_name],
        function (lesson) {
            if(lesson.subject.type == null){
                subjects = check(subjects, 'getSubjects');
                subjects.forEach(function (item) {
                    if(item.id == lesson.subject.id)lesson.subject = item;
                });
            }
            var tr = createTableLine([lesson.subject.name, lesson.subject.type.type, lesson.subject.teacher.name]);
            var td = document.createElement('td');
            td.textContent = lesson.date;
            tr.insertBefore(td, tr.firstChild);
            var checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.checked = lesson.isWas;
            checkbox.disabled = true;
            td = document.createElement('td');
            td.appendChild(checkbox);
            tr.insertBefore(td, tr.firstChild);
            tr.id = lesson.id;
            tr.childNodes[2].id = lesson.subject.id;
            td = document.createElement('td');
            if(thisUser == -2 || thisUser == lesson.subject.teacher.id) {
                var edit = createEditButton(lesson.id, table);
                td.appendChild(edit);
            }
            if(thisUser == -1 || thisUser == -2) {
                var deleteButton = createDeleteButton(lesson.id, table.requests.deleteRequest(lesson.id), table.requests.fieldsToClear);
                td.appendChild(deleteButton);
            }
            tr.appendChild(td);
            return tr;
        }, function (oldTableLine) {
            subjects = check(subjects, 'getSubjects');
            var ans = document.createElement('td');
            ans.colSpan = 5;
            var checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            var textField = document.createElement('input');
            textField.type = 'text';
            var select = document.createElement('select');
            var option;
            subjects.forEach(function (item) {
                option = document.createElement('option');
                option.value = item.id;
                option.textContent = item.name;
                select.appendChild(option);
            });
            if(oldTableLine != null) {
                textField.value =  oldTableLine.childNodes[1].childNodes[0].textContent;
                checkbox.checked = oldTableLine.childNodes[0].childNodes[0].checked;
                option = document.createElement('option');
                option.value = oldTableLine.childNodes[2].id;
                option.textContent = oldTableLine.childNodes[2].textContent;
                select.appendChild(option);
            }
            option.selected = true;
            var v = document.createElement('table');
            var tr = document.createElement('tr');
            var td = document.createElement('td');
            td.appendChild(checkbox);
            tr.appendChild(td);
            td = document.createElement('td');
            td.appendChild(textField);
            tr.appendChild(td);
            td = document.createElement('td');
            td.appendChild(select);
            tr.appendChild(td);
            v.appendChild(tr);
            ans.appendChild(v);
            return ans;
        }, function (oldTableLine, newTableLine) {
            var lesson = newTableLine.childNodes[0].childNodes[0].childNodes[0];
            var subjectIndex = lesson.childNodes[2].childNodes[0].selectedIndex;
            var selectedSubject = lesson.childNodes[2].childNodes[0][subjectIndex].textContent;
            oldTableLine.childNodes[0].checked = lesson.childNodes[0].childNodes[0].checked;
            oldTableLine.childNodes[1].textContent = lesson.childNodes[1].childNodes[0].value;
            oldTableLine.childNodes[2].textContent = selectedSubject;
            oldTableLine.childNodes[2].id = lesson.childNodes[1].childNodes[0].value;
            drawSchedule();
        }, function (tableLine) {
            var lesson =  tableLine.childNodes[0].childNodes[0].childNodes[0];
            var ans = {id:tableLine.id, isWas:lesson.childNodes[0].childNodes[0].checked, date:lesson.childNodes[1].childNodes[0].value, subject:{id:lesson.childNodes[2].childNodes[0].value} };
            return ans;
        }, new Request(
            function (lesson) {return 'createLesson?subject_id=' + encodeURIComponent(lesson.subject.id) + '&date=' + encodeURIComponent(lesson.date) + '&isWas=' + encodeURIComponent(lesson.isWas);},
            function () {return 'getSchedule'},
            function (lesson) {return 'updateLesson?id=' + encodeURIComponent(lesson.id) + '&subject_id=' + encodeURIComponent(lesson.subject.id) + '&date=' + encodeURIComponent(lesson.date) + '&isWas=' + encodeURIComponent(lesson.isWas);},
            function (id) {return 'deleteLesson?id=' + id},
            [schedule]), canCreate
    );
    drawTable(table);
    var forReplace = document.getElementById('forReplace');
    var ans = 0;
    schedule = check(schedule, 'getSchedule');
    schedule.forEach(function (item) {
        if(item.isWas == true)ans++;
    });
    var td = document.createElement('td');
    td.colSpan = 6;
    td.textContent = strings.string_total_lessons + ' : ' + schedule.length + ', ' + strings.string_total_lessons_was + ' : ' + ans/schedule.length*100 + '%';
    var tr = document.createElement('tr');
    tr.appendChild(td);
    forReplace.appendChild(tr);
}


function check(obj, request) {
    if(obj == null){
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'api', false);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(request);
        if(xhr.status != 200)
            alert(xhr.status + ' : ' + xhr.statusText);
        return JSON.parse(xhr.responseText);
    }else{
        return obj;
    }
}

function drawPermissionsTable() {
    var table = new Table(permissions, [strings.culumn_name_subject_type, strings.column_name_teacher_rang],
        function (permission) {
            var tr = createTableLine([permission.subjectType.type, permission.teacherRang.rang]);
            tr.id = permission.subjectType.id + ' ' + permission.teacherRang.id;
            var deleteButton = createDeleteButton(permission.subjectType.id + ' ' + permission.teacherRang.id, table.requests.deleteRequest(permission.subjectType.id + ' ' + permission.teacherRang.id), table.requests.fieldsToClear);
            var td = document.createElement('td');
            td.appendChild(deleteButton);
            tr.appendChild(td);
            return tr;
        }, function (oldTableLine) {
            subjectsType = check(subjectsType, 'getSubjectTypes');
            teachersRang = check(teachersRang, 'getTeachersRang');
            var ans = document.createElement('td');
            ans.colSpan = 2;
            var select = document.createElement('select');
            var option;
            subjectsType.forEach(function (item) {
                option = document.createElement('option');
                option.value = item.id;
                option.textContent = item.type;
                select.appendChild(option);
            });
            select.selectedIndex = 0;
            var selectTeacherRang = document.createElement('select');
            teachersRang.forEach(function (item) {
                option = document.createElement('option');
                option.value = item.id;
                option.textContent = item.rang;
                selectTeacherRang.appendChild(option);
            });
            selectTeacherRang.selectedIndex = 0;
            var v = document.createElement('table');
            var tr = document.createElement('tr');
            var td = document.createElement('td');
            td.appendChild(select);
            tr.appendChild(td);
            td = document.createElement('td');
            td.appendChild(selectTeacherRang);
            tr.appendChild(td);
            v.appendChild(tr);
            ans.appendChild(v);
            return ans;
        }, function (oldTableLine, newTableLine) {
            var subjectType = newTableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0];
            var selectedSubjectTypeIndex = subjectType.selectedIndex;
            var selectedSubjectType  = subjectType.options[selectedSubjectTypeIndex].textContent;
            var selectedSubjectTypeId = subjectType.value;
            var teacherRang = newTableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[1].childNodes[0];
            var selectedTeacherRangIndex = teacherRang.selectedIndex;
            var selectedTeacherRang  = teacherRang.options[selectedTeacherRangIndex].textContent;
            var selectedTeacherRangId = teacherRang.value;
            oldTableLine.childNodes[0].textContent = selectedSubjectType;
            oldTableLine.childNodes[0].id = selectedSubjectTypeId;
            oldTableLine.childNodes[1].textContent = selectedTeacherRang;
            oldTableLine.childNodes[1].id = selectedTeacherRangId;
        }, function (tableLine) {
            var selectedSubjectTypeId = tableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].value;
            var selectedTeacherRangId = tableLine.childNodes[0].childNodes[0].childNodes[0].childNodes[1].childNodes[0].value;
            var ans = {id:tableLine.id, subjectType:{id:selectedSubjectTypeId}, teacherRang:{id:selectedTeacherRangId}};
            return ans;
        }, new Request(
            function (permission) {return 'createPermission?subject_type_id=' + encodeURIComponent(permission.subjectType.id) + '&teacher_rang_id=' + encodeURIComponent(permission.teacherRang.id);},
            function () {return 'getPermissions'},
            function (permission) {},
            function (id) {return 'deletePermission?id=' + encodeURIComponent(id)},
            []), true
    );
    drawTable(table);
}


function createEditButton(id, table) {
    var edit = document.createElement('button');
    edit.appendChild(document.createTextNode(strings.button_name_edit));
    edit.classList.add('editButton');
    edit.onclick = function () {
        var tableLine = document.getElementById(id + '');
        var newTableLine = document.createElement('tr');
        newTableLine.id = id;
        newTableLine.append(table.getReplaceFieldsForEditFunc(tableLine));

        var buttonCancel = document.createElement('button');
        buttonCancel.classList.add('deleteButton');
        buttonCancel.textContent = strings.dialog_cancel;
        buttonCancel.onclick = function () {
            newTableLine.parentNode.replaceChild(tableLine, newTableLine);
        };

        var buttonOk = document.createElement('button');
        buttonOk.onclick = function () {
            var request = table.requests.updateRequest(table.getObjectForRequest(newTableLine));
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'api', false);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(request);
            if(xhr.responseText == "true" || xhr.responseText == "1"){
                popUpMessage(0, strings.dialog_edited_successfully);
                table.requests.fieldsToClear.forEach(function (item) {
                    item = null;
                });
                table.getReplaceFieldsFunc(tableLine, newTableLine);
                newTableLine.parentNode.replaceChild(tableLine, newTableLine);
            }else popUpMessage(1, xhr.responseText);
        };
        buttonOk.textContent = strings.dialog_confirm;

        var td = document.createElement('td');
        td.appendChild(buttonOk);
        td.appendChild(buttonCancel);
        newTableLine.appendChild(td);

        tableLine.parentNode.replaceChild(newTableLine, tableLine);
    };
    return edit;
}

function createDeleteButton(id, deleteRequest, fieldsToClear){
    var ans = document.createElement('button');
    ans.setAttribute('class', 'deleteButton');
    ans.appendChild(document.createTextNode(strings.button_name_delete));
    ans.onclick = function () {
        var tableLine = document.getElementById(id + '');
        if(confirm(strings.dialog_confirm_delete)){
            var request = deleteRequest;
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'api', false);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(request);
            if(xhr.responseText == "true" || xhr.responseText == "1"){
                popUpMessage(0, strings.dialog_deleted_successfully);
                fieldsToClear.forEach(function (item) {
                    item = null;
                });
                tableLine.parentNode.removeChild(tableLine);
            }else popUpMessage(1, xhr.responseText);
        }
    };
    return ans;
}

function createEntryButton(table) {
    var ans = document.createElement('tr');
    var td = document.createElement('td');
    td.colSpan = table.headers.length+1;
    var button = document.createElement('button');
    button.setAttribute('class', 'addButton');
    button.appendChild(document.createTextNode(strings.button_name_create));
    button.onclick = function () {
        var newTableLine = document.createElement('tr');
        newTableLine.append(table.getReplaceFieldsForEditFunc());

        var buttonCancel = document.createElement('button');
        buttonCancel.classList.add('deleteButton');
        buttonCancel.textContent = strings.dialog_cancel;
        buttonCancel.onclick = function () {
            newTableLine.parentNode.replaceChild(ans, newTableLine);
        };

        var buttonOk = document.createElement('button');
        buttonOk.textContent = strings.dialog_confirm;
        buttonOk.onclick = function () {
            var request = table.requests.createRequest(table.getObjectForRequest(newTableLine));
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'api', false);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(request);
            if(!isNaN(xhr.responseText)) {
                popUpMessage(0, strings.dialog_created_successfully);
                var object = table.getObjectForRequest(newTableLine);
                object.id = xhr.responseText;
                var tableLine = table.getTableLine(object);
                table.getReplaceFieldsFunc(tableLine, newTableLine);
                newTableLine.parentNode.replaceChild(tableLine, newTableLine);
                tableLine.parentNode.appendChild(ans);
            } else popUpMessage(1, xhr.responseText);
        };
        var td = document.createElement('td');
        td.appendChild(buttonOk);
        td.appendChild(buttonCancel);
        newTableLine.appendChild(td);
        ans.parentNode.replaceChild(newTableLine, ans);
    };
    td.appendChild(button);
    ans.appendChild(td);
    return ans;
}

function popUpMessage(status, text) {
    alert(text);
}

strings = check(strings, 'getStrings');
document.getElementById('thisAccount').textContent = strings.string_choose_account;
document.getElementById('signOutButton').textContent = strings.button_name_sign_out;
var xhr = new XMLHttpRequest();
xhr.open('POST', 'api', true);
xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
var body = 'init';
xhr.send(body);
xhr.onload =  function (){
    if (xhr.status !== 200) {
        alert( xhr.status + ': ' + xhr.statusText );
    } else {
        var accounts = JSON.parse(xhr.responseText);
        var accountList = document.createElement('ul');
        accountList.id ='forReplace';
        accounts.forEach(function (item) {
            var itemLink = document.createElement('a');
            var listItem = document.createElement('li');
            itemLink.href = 'javascript:chooseUser(' + JSON.stringify(item) +');';
            itemLink.appendChild(document.createTextNode(item.name));
            listItem.appendChild(itemLink);
            accountList.appendChild(listItem);
        });
        var forReplace = document.getElementById('forReplace');
        forReplace.parentNode.replaceChild(accountList, forReplace);
    }
};

function chooseUser(user) {
    var thisAccount = document.getElementById('thisAccount');
    thisAccount.innerText = '';
    thisAccount.appendChild(document.createTextNode(user.name));
    var singOutButton = document.getElementById('signOutButton');
    singOutButton.style.display = 'inline-block';
    switch(user.id){
        case -2:
            createButton( 'drawSubjectTypesTable()', strings.table_name_subjects_types);
            createButton( 'drawTeachersRangTable()', strings.table_name_teachers_rangs);
            createButton('drawPermissionsTable()', strings.table_name_permissions);
        case -1:
            createButton( 'drawSubjectsTable()', strings.table_name_subjects);
            createButton( 'drawTeachersTable()', strings.table_name_teachers);
        default:
            createButton( 'drawSchedule()', strings.table_name_schedule);
    }
    thisUser = user.id;
    drawSchedule();
}

function createButton(onclick, title) {
    var button = document.createElement('button');
    button.appendChild(document.createTextNode(title));
    button.setAttribute('onclick', onclick);
    document.getElementById('divWithButtons').appendChild(button);
}
