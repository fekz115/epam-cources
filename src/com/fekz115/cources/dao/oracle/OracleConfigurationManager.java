package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.util.ResourceBundle;

final class OracleConfigurationManager {

    static String user;
    static String password;
    static String url;

    static class SQLRequests {
        public static String teachersGetAll;
        public static String teachersGetById;
        public static String teachersUpdate;
        public static String teachersAdd;
        public static String teachersGetLast;
        public static String teachersDelete;

        public static String lessonsGetAll;
        public static String lessonsGetById;
        public static String lessonsUpdate;
        public static String lessonsAdd;
        public static String lessonsGetLast;
        public static String lessonsDelete;

        public static String subjectsGetAll;
        public static String subjectsGetById;
        public static String subjectsUpdate;
        public static String subjectsAdd;
        public static String subjectsGetLast;
        public static String subjectsDelete;

        public static String subjectTypeGetAll;
        public static String subjectTypeGetById;
        public static String subjectTypeUpdate;
        public static String subjectTypeAdd;
        public static String subjectTypeGetLast;
        public static String subjectTypeDelete;

        public static String teacherRangGetAll;
        public static String teacherRangGetById;
        public static String teacherRangUpdate;
        public static String teacherRangAdd;
        public static String teacherRangGetLast;
        public static String teacherRangDelete;

        public static String permissionGetAll;
        public static String permissionAdd;
        public static String permissionDelete;

        public static String checkTeacherRang;

        public static String subjectsGetByTeacherId;

    }
    private static SQLRequests sqlRequests;

    static{

        MainServlet.log.log(Level.INFO, "OracleConfigurationManager", "init");

        ResourceBundle resourceBundle = ResourceBundle.getBundle("server");

        user     = resourceBundle.getString("user");
        password = resourceBundle.getString("password");
        url      = resourceBundle.getString("url") + resourceBundle.getString("params");

        resourceBundle = ResourceBundle.getBundle("SQLrequests");
        try {
            for(var f : SQLRequests.class.getFields()){
                f.set(sqlRequests, resourceBundle.getString(f.getName()));
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /*public static OracleConfigurationManager getInstance() {
        if (instance == null) {
            synchronized (OracleConfigurationManager.class) {
                if (instance == null) {
                    instance = new OracleConfigurationManager();
                }
            }
        }
        return instance;
    }*/
}