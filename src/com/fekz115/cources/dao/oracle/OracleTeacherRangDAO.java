package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.dao.TeacherRangDAO;
import com.fekz115.cources.entities.TeacherRang;
import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OracleTeacherRangDAO extends OracleDAO implements TeacherRangDAO {

    @Override
    public List<TeacherRang> getAll() {
        try {
            List<TeacherRang> teacherRangsList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teacherRangGetAll);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                teacherRangsList.add(getTeacherRang(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return teacherRangsList;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public TeacherRang getById(int id) {
        TeacherRang ans = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teacherRangGetById);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            ans = getTeacherRang(resultSet);
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return ans;
    }

    @Override
    public boolean update(TeacherRang teacherRang) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teacherRangUpdate);
            preparedStatement.setString(1, teacherRang.rang);
            preparedStatement.setInt(2, teacherRang.id);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    @Override
    public int add(TeacherRang teacherRang) {
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teacherRangAdd);
            preparedStatement.setString(1, teacherRang.rang);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teacherRangGetLast);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            teacherRang.id = resultSet.getInt(1);
            preparedStatement.close();
            return teacherRang.id;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return -1;
    }

    @Override
    public boolean delete(TeacherRang teacherRang) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teacherRangDelete);
            preparedStatement.setInt(1, teacherRang.id);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    private TeacherRang getTeacherRang(ResultSet resultSet) throws SQLException{
        return new TeacherRang(resultSet.getInt(1), resultSet.getString(2));
    }
}
