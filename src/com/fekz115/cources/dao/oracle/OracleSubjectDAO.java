package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.dao.SubjectDAO;
import com.fekz115.cources.entities.*;
import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OracleSubjectDAO extends OracleDAO implements SubjectDAO {
    @Override
    public List<Subject> getAll() {
        try {
            List<Subject> subjectList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsGetAll);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                subjectList.add(getSubject(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return subjectList;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public Subject getById(int id) {
        Subject ans = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsGetById);
            preparedStatement.setInt(1, id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            ans = getSubject(resultSet);
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return ans;
    }

    @Override
    public boolean update(Subject subject) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsUpdate);
            preparedStatement.setInt(1, subject.type.id);
            preparedStatement.setInt(2, subject.teacher.id);
            preparedStatement.setString(3, subject.name);
            preparedStatement.setInt(4, subject.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    @Override
    public int add(Subject subject) {
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsAdd);
            preparedStatement.setInt(1, subject.type.id);
            preparedStatement.setInt(2, subject.teacher.id);
            preparedStatement.setString(3, subject.name);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsGetLast);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            subject.id = resultSet.getInt(1);
            preparedStatement.close();
            return subject.id;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return -1;
    }

    @Override
    public boolean delete(Subject subject) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsDelete);
            preparedStatement.setInt(1, subject.id);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    private Subject getSubject(ResultSet resultSet) throws SQLException{
        return new Subject(resultSet.getInt(1), resultSet.getString(4), new SubjectType(resultSet.getInt(10), resultSet.getString(11)), resultSet.getInt(3) != 0 ? new Teacher(resultSet.getInt(5), resultSet.getString(6), resultSet.getInt(7) != 0 ? new TeacherRang(resultSet.getInt(8), resultSet.getString(9)) : null) : null);
    }

    @Override
    public List<Subject> getSubjectsByTeacherId(Teacher teacher) {
        try {
            List<Subject> subjectList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectsGetByTeacherId);
            preparedStatement.setInt(1, teacher.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                subjectList.add(getSubject(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return subjectList;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public boolean checkPermission(Permission permission) {
        List<Subject> subjectList = getAll();
        for(Subject subject : subjectList){
            if(permission.subjectType.id == subject.type.id && permission.teacherRang.id == subject.teacher.rang.id)return false;
        }
        return true;
    }
}
