package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.dao.TeacherDAO;
import com.fekz115.cources.entities.Teacher;
import com.fekz115.cources.entities.TeacherRang;
import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OracleTeacherDAO extends OracleDAO implements TeacherDAO {

    @Override
    public List<Teacher> getAll() {
        try {
            List<Teacher> teachersList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teachersGetAll);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                teachersList.add(getTeacher(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return teachersList;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }


    @Override
    public Teacher getById(int id) {
        Teacher ans = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teachersGetById);
            preparedStatement.setInt(1, id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            ans = getTeacher(resultSet);
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return ans;
    }

    @Override
    public boolean update(Teacher teacher) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teachersUpdate);
            preparedStatement.setString(1, teacher.name);
            preparedStatement.setInt(2, teacher.rang != null ? teacher.rang.id : 0);
            preparedStatement.setInt(3, teacher.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    @Override
    public int add(Teacher teacher) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teachersAdd);
            preparedStatement.setString(1, teacher.name);
            preparedStatement.setInt(2, teacher.rang != null ? teacher.rang.id : 0);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teachersGetLast);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            teacher.id = resultSet.getInt(1);
            preparedStatement.close();
            return teacher.id;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return -1;
    }

    @Override
    public boolean delete(Teacher teacher) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.teachersDelete);
            preparedStatement.setInt(1, teacher.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }


    private Teacher getTeacher(ResultSet resultSet) throws SQLException {
        return new Teacher(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3) != 0 ? new TeacherRang(resultSet.getInt(4), resultSet.getString(5)) : null);
    }
}
