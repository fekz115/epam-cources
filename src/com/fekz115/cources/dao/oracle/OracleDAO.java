package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public abstract class OracleDAO {

    // Import mysql:mysql-connector-java:8.0.12 from Maven
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";

    static Connection connection = null;

    static {
        try {
            Class.forName(JDBC_DRIVER);
            MainServlet.log.log(Level.INFO, "OracleDAO", "Trying to coonect to data base");
            connection = DriverManager.getConnection(OracleConfigurationManager.url, OracleConfigurationManager.user, OracleConfigurationManager.password);
            // We create connection to DB only one time after creating first object what extends this class and close it when we call method onExit()
        } catch (ClassNotFoundException | SQLException e) {
            MainServlet.log.log(Level.FATAL, "OracleDAO", e.getMessage());
        }
    }

    public static void onExit() throws SQLException {
        connection.close();
        MainServlet.log.log(Level.INFO, "OracleDAO", "OracleDAO terminated");
    }
}
