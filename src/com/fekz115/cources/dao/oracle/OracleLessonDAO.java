package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.dao.LessonDAO;
import com.fekz115.cources.entities.*;
import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OracleLessonDAO extends OracleDAO implements LessonDAO {
    @Override
    public List<Lesson> getAll() {

        try {
            List<Lesson> lessonList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.lessonsGetAll);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                lessonList.add(getLesson(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return lessonList;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public Lesson getById(int id) {
        Lesson ans = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.lessonsGetById);
            preparedStatement.setInt(1, id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            ans = getLesson(resultSet);
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return ans;
    }

    @Override
    public boolean update(Lesson lesson) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.lessonsUpdate);
            preparedStatement.setDate(1, lesson.getDate());
            preparedStatement.setInt(2, lesson.subject.id);
            preparedStatement.setBoolean(3, lesson.isWas);
            preparedStatement.setInt(4, lesson.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    @Override
    public int add(Lesson lesson) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.lessonsAdd);
            preparedStatement.setDate(1, lesson.getDate());
            preparedStatement.setInt(2, lesson.subject.id);
            preparedStatement.setBoolean(3, lesson.isWas);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.lessonsGetLast);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            lesson.id = resultSet.getInt(1);
            preparedStatement.close();
            return lesson.id;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return -1;
    }

    @Override
    public boolean delete(Lesson lesson) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.lessonsDelete);
            preparedStatement.setInt(1, lesson.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    private Lesson getLesson(ResultSet resultSet) throws SQLException{
        return new Lesson(resultSet.getInt(1),  resultSet.getDate(3), resultSet.getBoolean(4), new Subject(resultSet.getInt(5), resultSet.getString(8), resultSet.getInt(6) != 0 ? new SubjectType(resultSet.getInt(9), resultSet.getString(10)) : null, resultSet.getInt(7) != 0 ? new Teacher(resultSet.getInt(11), resultSet.getString(12), resultSet.getInt(13) != 0 ? new TeacherRang(resultSet.getInt(14), resultSet.getString(15)) : null) : null));
    }
}
