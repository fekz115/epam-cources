package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.dao.SubjectTypeDAO;
import com.fekz115.cources.entities.SubjectType;
import com.fekz115.cources.servlets.MainServlet;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OracleSubjectTypeDAO extends OracleDAO implements SubjectTypeDAO {

    @Override
    public List<SubjectType> getAll() {
        try {
            List<SubjectType> subjectList = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectTypeGetAll);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                subjectList.add(getSubjectType(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return subjectList;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public SubjectType getById(int id) {
        SubjectType ans = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectTypeGetById);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            ans = getSubjectType(resultSet);
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return ans;
    }

    @Override
    public boolean update(SubjectType subjectType) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectTypeUpdate);
            preparedStatement.setString(1, subjectType.type);
            preparedStatement.setInt(2, subjectType.id);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    @Override
    public int add(SubjectType subjectType) {
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectTypeAdd);
            preparedStatement.setString(1, subjectType.type);
            preparedStatement.execute();
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectTypeGetLast);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            subjectType.id = resultSet.getInt(1);
            preparedStatement.close();
            return subjectType.id;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return -1;
    }

    @Override
    public boolean delete(SubjectType subjectType) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.subjectTypeDelete);
            preparedStatement.setInt(1, subjectType.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    SubjectType getSubjectType(ResultSet resultSet) throws SQLException{
        return new SubjectType(resultSet.getInt(1), resultSet.getString(2));
    }
}
