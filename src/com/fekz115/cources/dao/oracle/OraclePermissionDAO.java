package com.fekz115.cources.dao.oracle;

import com.fekz115.cources.dao.PermissionDAO;
import com.fekz115.cources.dao.SubjectTypeDAO;
import com.fekz115.cources.entities.*;
import com.fekz115.cources.servlets.MainServlet;
import com.mysql.cj.protocol.Resultset;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class OraclePermissionDAO extends OracleDAO implements PermissionDAO {

    @Override
    public List<Permission> getAll() {
        try {
            List<Permission> permissions = new ArrayList<>();
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.permissionGetAll);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while(resultSet.next()){
                permissions.add(getPermission(resultSet));
            }
            resultSet.close();
            preparedStatement.close();
            return permissions;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public Permission getById(int id) {
        Permission ans = null;
        /*try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.permissionGetById);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            ans = getPermission(resultSet);
            resultSet.close();
            preparedStatement.close();} catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }*/
        return ans;
    }

    @Override
    public boolean update(Permission permission) {
        /*try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.permissionUpdate);
            preparedStatement.setInt(1, permission.subjectType.id);
            preparedStatement.setInt(2, permission.teacherRang.id);
            preparedStatement.execute();
            preparedStatement.close();
            return true;} catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }*/
        return false;
    }

    @Override
    public int add(Permission permission) {
        try {
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.permissionAdd);
            preparedStatement.setInt(1, permission.teacherRang.id);
            preparedStatement.setInt(2, permission.subjectType.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return 1;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return -1;
    }

    @Override
    public boolean delete(Permission permission) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.permissionDelete);
            preparedStatement.setInt(1, permission.teacherRang.id);
            preparedStatement.setInt(2, permission.subjectType.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            preparedStatement.close();
            return true;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    Permission getPermission(ResultSet resultSet) throws SQLException{
        return new Permission(new TeacherRang(resultSet.getInt(3), resultSet.getString(4)), new SubjectType(resultSet.getInt(5), resultSet.getString(6)));
    }

    @Override
    public boolean checkTeacherRang(Permission permission) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(OracleConfigurationManager.SQLRequests.checkTeacherRang);
            preparedStatement.setInt(1, permission.teacherRang.id);
            preparedStatement.setInt(2, permission.subjectType.id);
            MainServlet.log.log(Level.TRACE, this.getClass().getName(), preparedStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            boolean ans = resultSet.next();
            resultSet.close();
            preparedStatement.close();
            return ans;
        } catch (SQLException e) {
            MainServlet.log.log(Level.ERROR, this.getClass().getName(), e.getMessage());
        }
        return false;
    }

    @Override
    public boolean checkTeachersSubject(Teacher teacher, List<Subject> subjectList) {
        List<Permission> permissions = getAll();
        for (Subject s : subjectList){
            for(Permission p : permissions){
                if(p.check(teacher, s))return true;
            }
        }
        return false;
    }
}
