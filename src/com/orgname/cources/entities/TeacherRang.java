package com.orgname.cources.entities;

public class TeacherRang {
    private int id;
    private String rang;

    public TeacherRang(int id, String rang) {
        this.id = id;
        this.rang = rang;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRang() {
        return rang;
    }

    public void setRang(String rang) {
        this.rang = rang;
    }

    @Override
    public String toString() {
        return "TeacherRang{" +
                "id=" + id +
                ", rang='" + rang + '\'' +
                '}';
    }
}
