package com.orgname.cources.entities;


public class Teacher {
    private int id = -1;
    private String name;
    private TeacherRang rang;

    public Teacher(int id, String name, TeacherRang rang) {
        this.id = id;
        this.name = name;
        this.rang = rang;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeacherRang getRang() {
        return rang;
    }

    public void setRang(TeacherRang rang) {
        this.rang = rang;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rang=" + rang.toString() +
                '}';
    }
}