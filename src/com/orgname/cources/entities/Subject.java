package com.orgname.cources.entities;

public class Subject {

    private int id;
    private String name;
    private Teacher teacher;
    private SubjectType type;

    public Subject(int id, String name, SubjectType type, Teacher teacher) {
        this.id = id;
        this.name = name;
        this.teacher = teacher;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public SubjectType getType() {
        return type;
    }

    public void setType(SubjectType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", teacher=" + (teacher == null ? null : teacher.toString()) +
                ", type=" + (type == null ? null : type.toString()) +
                '}';
    }
}
