package com.orgname.cources.entities;

import java.sql.Date;

public class Lesson {

    private int id = -1;
    private Date date;
    private boolean isWas;
    private Subject subject;
    private String dateString;

    public Lesson(int id, Date date, boolean isWas, Subject subject) {
        this.id = id;
        this.date = date;
        this.isWas = isWas;
        this.subject = subject;
        dateString = date.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isWas() {
        return isWas;
    }

    public void setWas(boolean was) {
        isWas = was;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate(){
        return date;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", date=" + date.toString() +
                ", isWas=" + isWas +
                ", subject=" + subject.toString() +
                '}';
    }
}
