package com.orgname.cources.entities;

public class Permission {
    private TeacherRang teacherRang;
    private SubjectType subjectType;

    public Permission(TeacherRang teacherRang, SubjectType subjectType) {
        this.teacherRang = teacherRang;
        this.subjectType = subjectType;
    }

    public boolean check(Teacher teacher, Subject subject){
        return teacher.getRang().getId() == teacherRang.getId() && subject.getType().getId() == subjectType.getId();
    }

    public TeacherRang getTeacherRang() {
        return teacherRang;
    }

    public void setTeacherRang(TeacherRang teacherRang) {
        this.teacherRang = teacherRang;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "teacherRang=" + teacherRang +
                ", subjectType=" + subjectType +
                '}';
    }
}
