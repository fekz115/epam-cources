package com.orgname.cources.tools;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Tools {


    public static Map<String, String> getQueryMap(String query)
    {
        String[] params = query.substring(query.indexOf('?')+1).split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params)
        {
            try {
                String name = URLDecoder.decode(param.split("=")[0], "UTF-8");
                String value = URLDecoder.decode(param.split("=")[1], "UTF-8");
                map.put(name, value);
            }catch (ArrayIndexOutOfBoundsException e){

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public static Map<String, String> convertResourceBundleToMap(ResourceBundle resource) {
        Map<String, String> map = new HashMap<>();
        Enumeration<String> keys = resource.getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            map.put(key, resource.getString(key));
        }
        return map;
    }
}
