package com.orgname.cources.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.orgname.cources.dao.*;
import com.orgname.cources.entities.*;
import com.google.gson.Gson;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static com.orgname.cources.tools.Tools.convertResourceBundleToMap;

public class MainServlet extends HttpServlet {

    private DAOFactory daoFactory = DAOFactory.MySql;

    private LessonDAO lessonDAO = daoFactory.getLessonDAO();
    private TeacherDAO teacherDAO = daoFactory.getTeacherDAO();
    private SubjectDAO subjectDAO = daoFactory.getSubjectDAO();
    private SubjectTypeDAO subjectTypeDAO = daoFactory.getSubjectTypeDAO();
    private TeacherRangDAO teacherRangDAO = daoFactory.getTeacherRangDAO();
    private PermissionDAO permissionDAO = daoFactory.getPermissionDAO();
    private final Logger log = LogManager.getLogger(MainServlet.class.getSimpleName());
    private Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String requestBody;
        StringBuilder buffer = new StringBuilder();
        BufferedReader bufferedReader = req.getReader();
        String line;
        while ((line = bufferedReader.readLine()) != null)
            buffer.append(line);
        requestBody = buffer.toString();
        log.log(Level.TRACE, requestBody);
        req.getParameterMap();
        ResourceBundle resourceBundle = ResourceBundle.getBundle("strings_backend", req.getLocale());
        Map<String, String> params  = com.orgname.cources.tools.Tools.getQueryMap(requestBody);
        if(requestBody.equals("getStrings")){
            resp.getWriter().print(gson.toJson(convertResourceBundleToMap(ResourceBundle.getBundle("strings", req.getLocale()))));
        } else if(requestBody.equals("init")){
            ArrayList<Teacher> teachers = new ArrayList<>();
            teachers.add(new Teacher(-2, resourceBundle.getString("string_admin"), null));
            teachers.add(new Teacher(-1, resourceBundle.getString("string_head_of_department"), null));
            teachers.add(new Teacher(0, resourceBundle.getString("string_guest"),  null));
            List<Teacher> temp = teacherDAO.getAll();
            teachers.addAll(temp);
            resp.getWriter().print(gson.toJson(teachers));
        } else if(requestBody.equals("getTeachers")){
            resp.getWriter().print(gson.toJson(teacherDAO.getAll()));
        } else if(requestBody.equals("getSubjects")){
            resp.getWriter().print(gson.toJson(subjectDAO.getAll()));
        } else if(requestBody.equals("getSchedule")){
            resp.getWriter().print(gson.toJson(lessonDAO.getAll()));
        } else if(requestBody.equals("getSubjectTypes")){
            resp.getWriter().print(gson.toJson(subjectTypeDAO.getAll()));
        } else if(requestBody.equals("getPermissions")){
            resp.getWriter().print(gson.toJson(permissionDAO.getAll()));
        } else if(requestBody.equals("getTeachersRang")){
            resp.getWriter().print(gson.toJson(teacherRangDAO.getAll()));

        } else if(requestBody.contains("updateSubjectType")){
            SubjectType subjectType = new SubjectType(Integer.parseInt(params.get("id")), params.get("type"));
            resp.getWriter().print(subjectTypeDAO.update(subjectType));
        } else if(requestBody.contains("deleteSubjectType")) {
            SubjectType subjectType = new SubjectType(Integer.parseInt(params.get("id")), null);
            resp.getWriter().print(subjectTypeDAO.delete(subjectType));
        } else if(requestBody.contains("createSubjectType")){
            SubjectType subjectType = new SubjectType(0, params.get("type"));
            resp.getWriter().print(subjectTypeDAO.add(subjectType));

        }else if(requestBody.contains("updateTeacherRang")){
            TeacherRang teacherRang = new TeacherRang(Integer.parseInt(params.get("id")), params.get("rang"));
            resp.getWriter().print(teacherRangDAO.update(teacherRang));
        } else if(requestBody.contains("deleteTeacherRang")) {
            TeacherRang teacherRang = new TeacherRang(Integer.parseInt(params.get("id")), null);
            resp.getWriter().print(teacherRangDAO.delete(teacherRang));
        } else if(requestBody.contains("createTeacherRang")){
            TeacherRang teacherRang = new TeacherRang(0, params.get("rang"));
            resp.getWriter().print(teacherRangDAO.add(teacherRang));

        } else if(requestBody.contains("updateSubject")){
            Subject subject = new Subject(Integer.parseInt(params.get("id")), params.get("name"), new SubjectType(Integer.parseInt(params.get("subject_type_id")), null), new Teacher(Integer.parseInt(params.get("teacher_id")), null, null));
            boolean check = permissionDAO.checkTeacherRang(new Permission(teacherDAO.getById(subject.getTeacher().getId()).getRang(), new SubjectType(subject.getType().getId(), null)));
            if(check)
                resp.getWriter().print(subjectDAO.update(subject));
            else resp.getWriter().print("");
        }else if(requestBody.contains("createSubject")){
            Subject subject = new Subject(0, params.get("name"), new SubjectType(Integer.parseInt(params.get("subject_type_id")), null), new Teacher(Integer.parseInt(params.get("teacher_id")), null, null));
            if(permissionDAO.checkTeacherRang(new Permission(teacherDAO.getById(subject.getTeacher().getId()).getRang(), new SubjectType(subject.getType().getId(), null))))
                resp.getWriter().print(subjectDAO.add(subject));
            else resp.getWriter().print(resourceBundle.getString("err_this_teacher_not_allowed"));
        }else if(requestBody.contains("deleteSubject")){
            Subject subject = new Subject(Integer.parseInt(params.get("id")),null, null, null);
            resp.getWriter().print(subjectDAO.delete(subject));

        } else if(requestBody.contains("updateTeacher")){
            Teacher teacher = new Teacher(Integer.parseInt(params.get("id")), params.get("name"), new TeacherRang(Integer.parseInt(params.get("teacher_rang_id")), null));
            if(permissionDAO.checkTeachersSubject(teacher, subjectDAO.getSubjectsByTeacherId(teacher)))resp.getWriter().print(teacherDAO.update(teacher));
            else resp.getWriter().print(resourceBundle.getString("err_this_t_rang_not_allowed"));
        } else if(requestBody.contains("deleteTeacher")) {
            Teacher teacher = new Teacher(Integer.parseInt(params.get("id")), null, null);
            if(subjectDAO.getSubjectsByTeacherId(teacher).isEmpty())resp.getWriter().print(teacherDAO.delete(teacher));
            else resp.getWriter().print(resourceBundle.getString("err_cant_delete_this_teacher"));
        } else if(requestBody.contains("createTeacher")) {
            Teacher teacher = new Teacher(0, params.get("name"), new TeacherRang(Integer.parseInt(params.get("teacher_rang_id")), null));
            resp.getWriter().print(teacherDAO.add(teacher));
        } else if(requestBody.contains("createPermission")){
            Permission permission = new Permission(new TeacherRang(Integer.parseInt(params.get("teacher_rang_id")), null), new SubjectType(Integer.parseInt(params.get("subject_type_id")), null));
            if(permissionDAO.add(permission) == 1)resp.getWriter().print("1");
            else resp.getWriter().print(resourceBundle.getString("err_permission_exist"));
        } else if(requestBody.contains("deletePermission")){
            String[] s = params.get("id").split(" ");
            Permission permission = new Permission(new TeacherRang(Integer.parseInt(s[1]), null), new SubjectType(Integer.parseInt(s[0]), null));
            if(subjectDAO.checkPermission(permission)){
                permissionDAO.delete(permission);
                resp.getWriter().print("true");
            }
            else resp.getWriter().print(resourceBundle.getString("err_cant_delete_permission"));
        } else if(requestBody.contains("updateLesson")){
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date(dateFormat.parse(params.get("date")).getTime());
                Lesson lesson = new Lesson(Integer.parseInt(params.get("id")), date, Boolean.parseBoolean(params.get("isWas")), new Subject(Integer.parseInt(params.get("subject_id")), null, null, null));
                resp.getWriter().print(lessonDAO.update(lesson));
            } catch (ParseException e) {
                resp.getWriter().print(resourceBundle.getString("err_invalid_date"));
            }
        } else if(requestBody.contains("deleteLesson")) {
            Lesson lesson = new Lesson(Integer.parseInt(params.get("id")),new Date(0), true, null);
            resp.getWriter().print(lessonDAO.delete(lesson));
        } else if(requestBody.contains("createLesson")) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date(dateFormat.parse(params.get("date")).getTime());
                Lesson lesson = new Lesson(0, date, Boolean.parseBoolean(params.get("isWas")), new Subject(Integer.parseInt(params.get("subject_id")), null, null, null));
                resp.getWriter().print(lessonDAO.add(lesson));
            } catch (ParseException e) {
                resp.getWriter().print(resourceBundle.getString("err_invalid_date"));
            }
        }
    }

    @Override
    public void destroy() {
        log.log(Level.INFO, "Terminating servlet");
        daoFactory.onExit();
        super.destroy();
    }
}
