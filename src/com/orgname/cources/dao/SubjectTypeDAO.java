package com.orgname.cources.dao;

import com.orgname.cources.entities.SubjectType;

public interface SubjectTypeDAO extends DAO<SubjectType> {
}
