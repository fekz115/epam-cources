package com.orgname.cources.dao;

import com.orgname.cources.entities.Permission;
import com.orgname.cources.entities.Subject;
import com.orgname.cources.entities.Teacher;

import java.util.List;

public interface SubjectDAO extends DAO<Subject>{

    List<Subject> getSubjectsByTeacherId(Teacher teacher);

    boolean checkPermission(Permission permission);

}
