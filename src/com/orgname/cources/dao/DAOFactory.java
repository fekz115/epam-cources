package com.orgname.cources.dao;

import com.orgname.cources.dao.mysql.*;

import java.sql.SQLException;

public enum DAOFactory {

    MySql{
        @Override
        public TeacherDAO getTeacherDAO() { return new MySqlTeacherDAO();  }

        @Override
        public SubjectDAO getSubjectDAO() {
            return new MySqlSubjectDAO();
        }

        @Override
        public LessonDAO getLessonDAO() {
            return new MySqlLessonDAO();
        }

        @Override
        public SubjectTypeDAO getSubjectTypeDAO() { return new MySqlSubjectTypeDAO(); }

        @Override
        public TeacherRangDAO getTeacherRangDAO() { return new MySqlTeacherRangDAO(); }

        @Override
        public PermissionDAO getPermissionDAO() { return new MySqlPermissionDAO(); }

        @Override
        public void onExit() {
            try {
                MySqlDAO.onExit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    };

    public abstract TeacherDAO getTeacherDAO();
    public abstract SubjectDAO getSubjectDAO();
    public abstract LessonDAO  getLessonDAO();
    public abstract SubjectTypeDAO getSubjectTypeDAO();
    public abstract TeacherRangDAO getTeacherRangDAO();
    public abstract PermissionDAO getPermissionDAO();

    public abstract void onExit();
    // Our factory must create DAOs for all entities and have method for correct close it

}
