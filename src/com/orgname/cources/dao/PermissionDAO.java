package com.orgname.cources.dao;

import com.orgname.cources.entities.Permission;
import com.orgname.cources.entities.Subject;
import com.orgname.cources.entities.Teacher;

import java.util.List;

public interface PermissionDAO extends DAO<Permission>{

    boolean checkTeacherRang(Permission permission);

    boolean checkTeachersSubject(Teacher teacher, List<Subject> subjectList);
}
