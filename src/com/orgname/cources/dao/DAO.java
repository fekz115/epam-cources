package com.orgname.cources.dao;

import java.util.List;

// This object contains common CRUD commands.

public interface DAO<T> {

    List<T> getAll();

    T getById(int id);

    boolean update(T t);

    int add(T t);

    boolean delete(T t);

}

